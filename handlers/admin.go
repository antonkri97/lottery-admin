package handlers

import (
	"net/http"

	"gopkg.in/gin-gonic/gin.v1"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/antonkri97/lottery_admin/models"
)

// Numbers Возвращает текущие 3 цифры
func Numbers(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)
	numbers := models.NumbersDB{}

	err := db.C("plays").Find(nil).Select(bson.M{"numbers": 1, "_id": 0}).Sort("-$natural").Limit(-1).One(&numbers)
	if err != nil {
		c.Status(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, numbers)
}

// Players возвращает все игры текущего дня
func Players(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)
	players := models.Players{}

	err := db.C("plays").Find(nil).Select(bson.M{
		"_id":       0,
		"none":      1,
		"twoNums":   1,
		"threeNums": 1,
	}).Sort("-$natural").Limit(-1).One(&players)

	if err != nil {
		c.Status(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, players)
}
