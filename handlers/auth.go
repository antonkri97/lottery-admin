package handlers

import (
	"net/http"

	"golang.org/x/crypto/bcrypt"
	"gopkg.in/gin-gonic/gin.v1"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/antonkri97/lottery_admin/models"
	"github.com/antonkri97/lottery_admin/utils"
)

// Ping for test request
func Ping(c *gin.Context) {
	c.String(http.StatusOK, "pong\n")
}

// Login авторизация
func Login(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)

	jsonUser := models.Admin{}
	dbUser := models.Admin{}

	if c.BindJSON(&jsonUser) == nil {
		err := db.C("admin").Find(bson.M{"username": jsonUser.Username}).One(&dbUser)
		if err == nil {
			err = bcrypt.CompareHashAndPassword([]byte(dbUser.Password), []byte(jsonUser.Password))
			if err == nil {
				c.JSON(http.StatusOK, bson.M{"token": utils.GenerateToken()})
				c.Status(http.StatusOK)
			} else {
				c.Status(http.StatusUnauthorized)
			}
		} else {
			c.Status(http.StatusUnauthorized)
		}
	} else {
		c.Status(http.StatusInternalServerError)
	}
}
