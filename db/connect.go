package db

import (
	"fmt"

	"github.com/antonkri97/lottery_admin/utils"
	"gopkg.in/mgo.v2"
)

var (
	// Session stores mongo session
	Session *mgo.Session

	// Mongo stores the mongodb connection to the db
	Mongo *mgo.DialInfo
)

const (
	// MongoDBUrl is the default mongodb url that will be used to connect to the
	// database.
	MongoDBUrl = "mongodb://192.168.99.100:27017/lottery"
	// UserNameDB is the default username for local db
	UserNameDB = "lottery"
	// PasswordDB is the default password for local db
	PasswordDB = "0000"
)

// Connect connects to mongodb
func Connect() {
	uri := utils.CheckEnv("MONGODB_URL", MongoDBUrl)

	mongo, err := mgo.ParseURL(uri)
	s, err := mgo.Dial(uri)
	if err != nil {
		fmt.Printf("Can't connect to mongo: %v\n", err)
		panic(err.Error())
	}

	usr := utils.CheckEnv("USERNAME_DB", UserNameDB)
	pwd := utils.CheckEnv("PASSWORD_DB", PasswordDB)

	s.DB("lottery").Login(usr, pwd)
	if err != nil {
		fmt.Printf("Can't login to mongo: %v\n", err)
		panic(err.Error())
	}

	s.SetSafe(&mgo.Safe{})
	fmt.Println("Connected to ", uri)
	Session = s
	Mongo = mongo
}
