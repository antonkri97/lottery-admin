package models

// NumbersDB модель главных игральных цифр
type NumbersDB struct {
	Numbers struct {
		First  int `bson:"fst"`
		Second int `bson:"snd"`
		Third  int `bson:"thd"`
	}
}
