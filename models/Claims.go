package models

import jwt "github.com/dgrijalva/jwt-go"

// Claims for jwt
type Claims struct {
	Username string `json:"username"`
	// recommended having
	jwt.StandardClaims
}
