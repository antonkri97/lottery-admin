package models

// PlayerStat это показатели конкретного игрока
type PlayerStat struct {
	First  int    `bson:"fst"`
	Second int    `bson:"snd"`
	Third  int    `bson:"thd"`
	Email  string `bson:"email"`
}

// Players это объект, содержащий массивы всех игр текущего игрового дня
type Players struct {
	None      []PlayerStat `bson:"none"`
	TwoNums   []PlayerStat `bson:"twoNums"`
	ThreeNums []PlayerStat `bson:"threeNums"`
}
