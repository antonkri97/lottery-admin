package models

// Admin модель админа в MongoDB
type Admin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
