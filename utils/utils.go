package utils

import "os"

// CheckEnv возвращает значение по-умолчанию, если переменная окружения не опеределена
// либо саму переменную окружения
func CheckEnv(env string, def string) string {
	e := os.Getenv(env)
	if len(e) == 0 {
		return def
	}
	return e
}
