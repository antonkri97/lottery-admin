FROM golang:latest

ADD . /go/src/github.com/antonkri97/lottery_admin

ENV GOBIN /go/bin

RUN go get gopkg.in/gin-gonic/gin.v1 && go get gopkg.in/mgo.v2 && go get golang.org/x/crypto/bcrypt && go get github.com/dgrijalva/jwt-go && go install /go/src/github.com/antonkri97/lottery_admin/main.go

EXPOSE 8080

CMD ["/go/bin/main"]
