package main

import (
	"github.com/antonkri97/lottery_admin/db"
	"github.com/antonkri97/lottery_admin/handlers"
	"github.com/antonkri97/lottery_admin/middlewares"
	"github.com/antonkri97/lottery_admin/utils"
	"gopkg.in/gin-gonic/gin.v1"
)

// Port is default port
const (
	Port = "8080"
)

func init() {
	db.Connect()
}

func main() {
	router := gin.New()

	router.Use(middlewares.Connect)
	router.Use(middlewares.CORSMiddleware())

	authorized := router.Group("/")

	authorized.Use(middlewares.AuthRequired)
	{
		authorized.GET("/ping", handlers.Ping)
		authorized.GET("/numbers", handlers.Numbers)
		authorized.GET("/plays", handlers.Players)
	}

	router.POST("/login", handlers.Login)

	port := utils.CheckEnv("PORT", "8080")

	router.Run(":" + port)
}
